from automic.table import TableModel
from automic.utils import FileUtils
from java.io import File

from loci.formats import ImageReader
from loci.formats import MetadataTools


#@ File (label="Input tablePath", value='W:/halavaty/tobias_kletter_test/exp1_2020/20200724-201611/summary_m-calcPositions.txt') inputTablePath



def calculateOffsets(_table):
    
    
    nRows=inputTable.getRowCount()
    
    xOffsetColumnName="Offset.X.Px"
    yOffsetColumnName="Offset.Y.Px"
    
    
    inputTable.addValueColumn(xOffsetColumnName,"NUM")
    inputTable.addValueColumn(yOffsetColumnName,"NUM")

    for rowIndex in range(0,nRows): #range(0,nRows)#range(0,1)

        #print rowIndex
        try:
            widthLZPixels=inputTable.getNumericValue(rowIndex,'LowZoomStitch.Width.Px')
            heightLZPixels=inputTable.getNumericValue(rowIndex,'LowZoomStitch.Height.Px')
            lzPixSizeX=inputTable.getNumericValue(rowIndex,'LowZoomStitch.X.Pixel.Size')
            lzPixSizeY=inputTable.getNumericValue(rowIndex,'LowZoomStitch.Y.Pixel.Size')
            lzStageX=inputTable.getNumericValue(rowIndex,'LowZoomStitch.Stage.X.Um')
            lzStageY=inputTable.getNumericValue(rowIndex,'LowZoomStitch.Stage.Y.Um')
            hzStageX=inputTable.getNumericValue(rowIndex,'AFHighZoom.Stage.X.Um')
            hzStageY=inputTable.getNumericValue(rowIndex,'AFHighZoom.Stage.Y.Um')
            
            xTopLeftLowZoom=lzStageX-lzPixSizeX*widthLZPixels/2.0
            yTopLeftLowZoom=lzStageY-lzPixSizeY*heightLZPixels/2.0
            
            offsetXPx=(hzStageX-xTopLeftLowZoom)/lzPixSizeX
            offsetYPx=(hzStageY-yTopLeftLowZoom)/lzPixSizeY
            
            inputTable.setNumericValue(offsetXPx,rowIndex,xOffsetColumnName)
            inputTable.setNumericValue(offsetYPx,rowIndex,yOffsetColumnName)
        except:
            print 'Row %d can not be processed' %rowIndex


if __name__ in ('__main__','__builtin__'):
    inputTable=TableModel(inputTablePath)

    calculateOffsets(_table=inputTable)



    outputTableName='%s-offsets.txt' %FileUtils.cutExtension(inputTablePath.getName())
    inputTable.writeNewFile(outputTableName,True)

    print 'Done!'