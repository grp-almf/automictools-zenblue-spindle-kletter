from automic.table import TableModel
from automic.utils import FileUtils
from java.io import File
from automic.online.microscope import ZeissLSM800

from loci.formats import ImageReader
from loci.formats import MetadataTools

from automic.geom import Point3D


#@ File (label="Input tablePath", value='W:/halavaty/tobias_kletter_test/exp1_2020/20200724-201611/summary_m-calcPositions-offsets.txt') inputTablePath


def unique(list1):
    unique_list = []
    for x in list1:
        if x not in unique_list:
            unique_list.append(x)
    
    return unique_list

def generateFeedbackFiles(_table):
    
    
    nRows=_table.getRowCount()
    
    
    lzFiles=list()

    for rowIndex in range(0,nRows): #range(0,nRows)#range(0,1)
        lzFiles.append(_table.getFileAbsolutePathString(rowIndex,'LowZoomStitch','IMG'))

    #print lzFiles.__len__()

    lzFiles=sorted(unique(lzFiles))
    
    #print lzFiles.__len__()

    for lzFile in lzFiles:
        points=list()
        try:
            for rowIndex in range(0,nRows):
                if _table.getFileAbsolutePathString(rowIndex,'LowZoomStitch','IMG')!=lzFile:
                    continue
                points.append(Point3D(_table.getNumericValue(rowIndex,'Offset.X.Px'),_table.getNumericValue(rowIndex,'Offset.Y.Px'),None))
            
            ZeissLSM800.submitJobPixels(File(lzFile), 'AFHighZoom', points)
        except:
            print '[WARNING:] File %s can not be processed' %lzFile


if __name__ in ('__main__','__builtin__'):
    ZeissLSM800.FEEDBACK_POSTFIX="_feedbackCalc.json"
    
    inputTable=TableModel(inputTablePath)

    generateFeedbackFiles(_table=inputTable)



    #outputTableName='%s-offsets.txt' %FileUtils.cutExtension(inputTablePath.getName())
    #inputTable.writeNewFile(outputTableName,True)

    print 'Done!'