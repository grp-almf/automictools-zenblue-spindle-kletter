from automic.table import TableModel
from automic.utils import FileUtils
from java.io import File

from loci.formats import ImageReader
from loci.formats import MetadataTools


#@ File (label="Input tablePath", value='W:/halavaty/tobias_kletter_test/exp1_2020/20200724-201611/summary_m.txt') inputTablePath

tagsToProcess='LowZoomStitch','AFHighZoom'

def getImageDimensions(_imageFilePath):
    if not File(_imageFilePath):
        return None

    reader = ImageReader();
    omeMeta = MetadataTools.createOMEXMLMetadata()
    reader.setMetadataStore(omeMeta)
    reader.setId(_imageFilePath)
    
    widthPix=omeMeta.getPixelsSizeX(0).getNumberValue()
    heightPix=omeMeta.getPixelsSizeY(0).getNumberValue()
    xPixSize=omeMeta.getPixelsPhysicalSizeX(0).value()
    yPixSize=omeMeta.getPixelsPhysicalSizeY(0).value()
    xStage=omeMeta.getStageLabelX(0).value()
    yStage=omeMeta.getStageLabelY(0).value()
    
    
    reader.close()
    
    return widthPix,heightPix,xPixSize,yPixSize,xStage,yStage


def extractPositionsToTable(_table,_imageTag):
    
    print('Processing %s images' %_imageTag)
    
    nRows=inputTable.getRowCount()
    
    widthPixColumnName="%s.Width.Px" %_imageTag
    heightPixColumnName="%s.Height.Px" %_imageTag
    xPixSizeColumnName="%s.X.Pixel.Size" %_imageTag
    yPixSizeColumnName="%s.Y.Pixel.Size" %_imageTag
    xStageColumnName="%s.Stage.X.Um" %_imageTag
    yStageColumnName="%s.Stage.Y.Um" %_imageTag
    
    inputTable.addValueColumn(widthPixColumnName,"NUM")
    inputTable.addValueColumn(heightPixColumnName,"NUM")
    inputTable.addValueColumn(xPixSizeColumnName,"NUM")
    inputTable.addValueColumn(yPixSizeColumnName,"NUM")
    inputTable.addValueColumn(xStageColumnName,"NUM")
    inputTable.addValueColumn(yStageColumnName,"NUM")

    for rowIndex in range(0,nRows): #range(0,nRows)#range(0,1)

        #print rowIndex

        imagePath=inputTable.getFileAbsolutePathString(rowIndex,_imageTag,'IMG')
        if imagePath is None:
            continue
        if imagePath=='':
            continue
        if not File(imagePath).exists():
            print "File %s does not exist. Script will continue"
            continue

        widthPix,heightPix,xPixSize,yPixSize,xStageUm,yStageUm=getImageDimensions(imagePath)

        inputTable.setNumericValue(widthPix,rowIndex,widthPixColumnName)
        inputTable.setNumericValue(heightPix,rowIndex,heightPixColumnName)
        inputTable.setNumericValue(xPixSize,rowIndex,xPixSizeColumnName)
        inputTable.setNumericValue(yPixSize,rowIndex,yPixSizeColumnName)
        inputTable.setNumericValue(xStageUm,rowIndex,xStageColumnName)
        inputTable.setNumericValue(yStageUm,rowIndex,yStageColumnName)


if __name__ in ('__main__','__builtin__'):
    inputTable=TableModel(inputTablePath)
    for imageTag in tagsToProcess:
        extractPositionsToTable(_table=inputTable,_imageTag=imageTag)



    outputTableName='%s-calcPositions.txt' %FileUtils.cutExtension(inputTablePath.getName())
    inputTable.writeNewFile(outputTableName,True)

    print 'Done!'