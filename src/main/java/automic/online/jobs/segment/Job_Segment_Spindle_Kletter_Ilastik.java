package automic.online.jobs.segment;

import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissLSM800;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
//import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ImageStatistics;

//ilastik imports
import org.ilastik.ilastik4ij.executors.AbstractIlastikExecutor.PixelPredictionType;
import org.ilastik.ilastik4ij.executors.PixelClassification;

//IJ2 imports
import net.imagej.ImageJ;
import net.imagej.ImgPlus;
import net.imagej.legacy.LegacyService;
import net.imglib2.img.Img;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;



public class Job_Segment_Spindle_Kletter_Ilastik extends Job_Default{
	public static final String INPUT_IMG_TAG="LowZoomStitch";
	
	private boolean selectionMade=false;
	

	public static final String KEY_CHANNEL_INDEX="Channel index";
	public static final String KEY_ILASTIK_PATH="Ilastik path";
	public static final String KEY_ILASTIK_SEGMENTATION_PATH="Ilastik segmentation project path";

	public static final String KEY_TARGET_CLASS="Target class";
	public static final String KEY_FILTER_RADIUS="Gaus filter radius";
	public static final String KEY_PROBABILITY_THRESHOLD="Probability threshold";
	public static final String KEY_AREA_PIXELS="Minimal area in pixels";
	public static final String KEY_MAX_OBJECTS="Maximum number of objects";
	public static final String KEY_ILASTIK_THREADS="Ilastik number of threads";
	public static final String KEY_ILASTIK_RAM="Ilastik maximum RAM (MB)";	

	
	
	private ImagePlus inImg;
	ImagePlus maxProjectedImage;
	

	//PIPELINE PARAMETERS
	private int chanelIndex=1;
	
	String targetClass="1";
	
	//min area in pixels
	double minimalAreaPixels = 400;
	
	double probabilityThreshold=0.5;
	
	double gausFilterRadius=3.0;
	
	int maxNObjects=5;
	
	int ilastikThreads=4;
	
	int ilastikRAM=4096;
	
	//Ilastik executable
	String ilastikPath = "C:\\Program Files\\ilastik-1.3.3post3\\run-ilastik.bat";
	
	//Ilastik segmentation project path
	String ilastikSegmentationProjectPath = "D:\\tempDat\\tobias_kletter_test\\20200727_spindledetection.ilp";
	
	Roi[] objectRois;
	Point3D[] positions;

	
	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_Segment_Spindle_Kletter_Ilastik(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		selectionMade=false;
		positions=null;
		objectRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		//currentTable.cleanRecord(curDInd);

		TimeUnit.MILLISECONDS.sleep(3000);
		inImg=ImageOpenerWithBioformats.openSingleChannel(newImgFile,0,chanelIndex,false);
		//selectedRoiManager= new SelectionManadgement();
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		inImg=ImageOpenerWithBioformats.openSingleChannel(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"),0,chanelIndex,false);
		//selectedRoiManager= new SelectionManadgement();
	}


	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		
		// removing scale to count everything in pixels
		IJ.run(inImg, "Set Scale...", "distance=0");
		
		showDebug(inImg, "OriginalImage", true);

		//Duplicate only first channel
		ImagePlus inImgCh1=new Duplicator().run(inImg, 1, 1, 1, inImg.getNSlices(), 1, inImg.getNFrames());
		
		//Z projection (if needed)
		if (inImg.getNSlices() > 1) {
			
			ZProjector projector=new ZProjector();
			projector.setMethod(ZProjector.MAX_METHOD);
			projector.setImage(inImgCh1);
			projector.doProjection();
			maxProjectedImage = projector.getProjection();
					
		} else {
			maxProjectedImage = inImgCh1;
		}
		showDebug(maxProjectedImage, "Max Projected Image", true);
		
		
		//I L A S T I K   S E G M E N T A T I O N 
		LegacyService legacyService=LegacyService.getInstance();
		
		//create PixelClassificator
		final PixelClassification pixelClassificator = new PixelClassification(
                new File(ilastikPath),
                new File(ilastikSegmentationProjectPath),
                legacyService.log(),
                legacyService.status(),
                ilastikThreads,
                ilastikRAM
        );
		
		Img<? extends RealType<?>> realImg = ImageJFunctions.wrapReal(maxProjectedImage);
		
		
		//run pixel classification
		final ImgPlus<FloatType> segmentedImageIJ2 = pixelClassificator.classifyPixels(new ImgPlus(realImg), PixelPredictionType.Probabilities);
		ImagePlus segmentedImage = ImageJFunctions.wrap(segmentedImageIJ2, "ilastik segmented image");
		
		showDebug(segmentedImage, "Classified Pixels", true);
		
		ImagePlus targetClassImage=new Duplicator().run(segmentedImage,1,1);
		

		
		
		showDebug(targetClassImage, "Target Class Image", true);

		IJ.run(targetClassImage,"Gaussian Blur...", String.format("sigma=%f", gausFilterRadius));
		showDebug(targetClassImage, "Filtered image", true);

		// threshold to get binary mask 
		// and dilate mask
		IJ.setThreshold(targetClassImage,probabilityThreshold, 1);
		IJ.run(targetClassImage,"Convert to Mask", "method=Default background=Default black");
		showDebug(targetClassImage, "Mask image", true);

		IJ.run(targetClassImage,"Dilate","");
		showDebug(targetClassImage, "Dilated Mask image", true);

		
		
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		IJ.setThreshold(targetClassImage, 128, 255);
		
		int part_opt=ParticleAnalyzer.SHOW_NONE|ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES|ParticleAnalyzer.ADD_TO_MANAGER;
		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,0,null,minimalAreaPixels,Double.MAX_VALUE,0,1);
		PartAn.setHideOutputImage(true);
		PartAn.analyze(targetClassImage,targetClassImage.getProcessor());
		
		objectRois=rm.getRoisAsArray();
		
		if(ROIManipulator2D.isEmptyRoiArr(objectRois))
			return false;
		
		if (objectRois.length>maxNObjects) {
			ParticleFilterer pFilterer =new ParticleFilterer(targetClassImage.getProcessor(), objectRois);
			pFilterer.selectRandomObjects(maxNObjects);
			objectRois=pFilterer.getPassedRois();
		}
		
		int nObjects=objectRois.length;
		positions=new Point3D[nObjects];
		for (int iObject=0;iObject<nObjects;iObject++) {
			targetClassImage.setRoi(objectRois[iObject]);
			ImageStatistics stat=targetClassImage.getStatistics(ImageStatistics.CENTROID);
			positions[iObject]=new Point3D(stat.xCentroid,stat.yCentroid,null);
		}
		
		return true;
	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if(!ROIManipulator2D.isEmptyRoiArr(objectRois))
			for (Roi r:objectRois)
				o.add(r);
		
		
		
		
		return o;
	}
	

	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(maxProjectedImage, getOverlay(), _xvis, _yvis);

		//if (selPoint==null) return;
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", positions);

		File roiSaveFolder=new File(newImgFile,"../../Metaphase.Roi").getCanonicalFile();
		if (!roiSaveFolder.isDirectory()) {
			roiSaveFolder.mkdir();
		}
		String roiNameNoExt=FileUtils.cutExtension(newImgFile.getName());
		ROIManipulator2D.saveRoisToFile(roiSaveFolder.getAbsolutePath(), roiNameNoExt, objectRois);
		currentTable.setFileAbsolutePath(new File(roiSaveFolder,roiNameNoExt+".zip"), curDInd, "Metaphase.Roi", "ROI");
		
		/*
		selectedRoiManager.updateCommandStrings();
		ZeissKeys.submitCommandsToMicroscope("trigger1",""+(inImg.getWidth()/2.0-1),""+(inImg.getHeight()/2.0-1),"0",selectedRoiManager.roiTypeCommandString,selectedRoiManager.roiAimCommandString, selectedRoiManager.xCommandString, selectedRoiManager.yCommandString,"");
		IJ.log(String.format("Launcing photomanipulation imaging job with %d selected regions", this.selectedRoiManager.selectedRegions.size()));
		
		int nRois=selectedRoiManager.selectedRegions.size();
		if (nRois>0) {
			Roi[] selectedRegionsArray=selectedRoiManager.selectedRegions.toArray(new Roi[nRois]);
		
			saveRoisForImage(newImgFile, selectedRegionsArray);
		}
		*/
		
		
	}
	
	@Override
	public void postProcessFail()throws Exception{
		//super.postProcessFail();
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", positions);
		
		
		IJ.log("No region was selected. Continue with selection imaging job");

	}

	@Override
	public void postProcessOffline(){
		try {
			ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", positions);
		}catch(Exception ex) {
			System.out.println("Exception");
		}
		IJ.log("Selection made "+selectionMade);
		//if (selectionMade)
		//	IJ.log("Number of selected ROIs " + this.selectedRoiManager.selectedRegions.size());
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_CHANNEL_INDEX, null, chanelIndex, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_ILASTIK_PATH, null, ilastikPath, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_ILASTIK_SEGMENTATION_PATH, null, ilastikSegmentationProjectPath, ParameterType.STRING_PARAMETER);

		jobCollection.addParameter(KEY_ILASTIK_THREADS, null, ilastikThreads, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_ILASTIK_RAM, null, ilastikRAM, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_TARGET_CLASS, null, targetClass, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_FILTER_RADIUS, null, gausFilterRadius, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_PROBABILITY_THRESHOLD, null, probabilityThreshold, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_AREA_PIXELS, null, minimalAreaPixels, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_MAX_OBJECTS, null, maxNObjects, ParameterType.INT_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){

		this.chanelIndex=(Integer)_jobParameterCollection.getParameterValue(KEY_CHANNEL_INDEX);
		this.ilastikPath=(String)_jobParameterCollection.getParameterValue(KEY_ILASTIK_PATH);
		this.ilastikSegmentationProjectPath=(String)_jobParameterCollection.getParameterValue(KEY_ILASTIK_SEGMENTATION_PATH);

		this.ilastikThreads=(Integer)_jobParameterCollection.getParameterValue(KEY_ILASTIK_THREADS);
		this.ilastikRAM=(Integer)_jobParameterCollection.getParameterValue(KEY_ILASTIK_RAM);
		
		this.targetClass=(String)_jobParameterCollection.getParameterValue(KEY_TARGET_CLASS);
		this.gausFilterRadius=(Double)_jobParameterCollection.getParameterValue(KEY_FILTER_RADIUS);
		this.probabilityThreshold=(Double)_jobParameterCollection.getParameterValue(KEY_PROBABILITY_THRESHOLD);
		this.minimalAreaPixels=(Double)_jobParameterCollection.getParameterValue(KEY_AREA_PIXELS);
		this.maxNObjects=(Integer)_jobParameterCollection.getParameterValue(KEY_MAX_OBJECTS);
		
		
	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-3, 10,10,3);
	}

	
	
	
	public static void main(String[] args)throws Exception{
		// start ImageJ
		//plugin directory needed for certain MorphoLibJ operations, such as Distance Transform Watershed

		//final String pluginDir="D:\\Alex_work\\running_soft\\Fiji.app_2019\\plugins";
		//final String pluginDir="/home/cosenza/Programs/fiji-linux64/Fiji.app/plugins";
		
		//final String tblAPth="D:\\tempDat\\Marco_Cosenza\\test-micronuclei-identification\\summary_manual.txt";
		final String tblAPth="D:\\tempDat\\tobias_kletter_test\\20200724-201611sub/summary_m.txt";

		//Images with detectable Spindles are:
		final String imgFnm="LowZoomStitch--W0000--P0002-T0093.czi";
		
		//System.getProperties().setProperty("plugins.dir", pluginDir);
		
		new ImageJ().ui().showUI();
		
		//new ij.ImageJ();
		
		Job_Segment_Spindle_Kletter_Ilastik testJob=new Job_Segment_Spindle_Kletter_Ilastik();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}
	

}
