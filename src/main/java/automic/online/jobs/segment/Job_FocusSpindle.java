package automic.online.jobs.segment;

import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.online.microscope.ZeissLSM800;
import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.frame.RoiManager;
import ij.process.ImageStatistics;

public class Job_FocusSpindle  extends Job_Default{

	public static final String INPUT_IMG_TAG="AFHighZoom";

	
	public static final String KEY_WAIT_IMAGE_OPENING="Wait for image opening (milliseconds)";
	public static final String KEY_USE_TEMPLATE_MATCHING="Use template matching";
	public static final String KEY_TEMPLATE_PROBABILITY_THRESHOLD="Template probability threshold";
	public static final String KEY_TEMPLATE_FILE_PATH="Template file path";

	
	
	private ImagePlus img=null;
	
	private Point3D selectedPosition;
	
	private Roi boxRoi;

	//parameters
	private int imageOpeningTimeDelay=1000;
	private boolean useTemplateMatching=false;
	private double probabilityThreshold=0.5;
	private String templateFilePath="D:\\tempDat\\tobias_kletter_test\\LZ-templates\\HZ-template.tif";
	
	
	@Override	
	public void initialise(String _fTag,String _colNm, boolean _visOnline){
		super.initialise(_fTag, _colNm, _visOnline);
	}

	
	@Override
	protected void cleanIterOutput(){
		img=null;
		selectedPosition=null;
		boxRoi=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		TimeUnit.MILLISECONDS.sleep(imageOpeningTimeDelay);
		img=ImageOpenerWithBioformats.openImage(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=ImageOpenerWithBioformats.openImage(newImgFile);
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		
		// removing scale to count everything in pixels
		IJ.run(img, "Set Scale...", "distance=0");
		
		this.showDebug(img, "Original Image", true);


		//define Z
		double maxv=0;
		int maxind=-1;
		
		ImageStack stack=img.getStack();
		int nSlices=stack.getSize();
		
		double currentValue;
		for (int i=0;i<nSlices;i++){
			currentValue=stack.getProcessor(i+1).getStatistics().mean;
			if (currentValue>maxv){
				maxind=i;
				maxv=currentValue;
			}
		}
		
		//define XY
		ImagePlus zFocusImage=new Duplicator().run(img, 1, 1, maxind+1, maxind+1, 1, 1);

		this.showDebug(zFocusImage, "Z focus image", true);
		
		if (useTemplateMatching) {
			zFocusImage.setTitle("zFocusImage");
			zFocusImage.show();
			
			ImagePlus tempImage=IJ.openImage(templateFilePath);
			tempImage.show();
			tempImage.setTitle("TempImage");

			
			RoiManager rm=ROIManipulator2D.getEmptyRm();
			IJ.run("Template Matching Image",String.format("template=TempImage image=zFocusImage flip_template_vertically flip_template_horizontally rotate=[30, 60, 90, 120, 150] matching_method=[Normalised 0-mean cross-correlation] number_of_objects=1 score_threshold=%f maximal_overlap=0.2 add_roi", probabilityThreshold));
			zFocusImage.hide();
			tempImage.hide();
			
			
			boxRoi=rm.getRoisAsArray()[0];
			zFocusImage.setRoi(boxRoi);
			ImageStatistics stat=zFocusImage.getStatistics(ImageStatistics.CENTER_OF_MASS);
			selectedPosition=new Point3D(stat.xCenterOfMass, stat.yCenterOfMass, (double)maxind);
			zFocusImage.setRoi((Roi)null);

			
		}else{
			ImageStatistics stat=zFocusImage.getStatistics(ImageStatistics.CENTER_OF_MASS);
			selectedPosition=new Point3D(stat.xCenterOfMass, stat.yCenterOfMass, (double)maxind);
		}
		
		if(Debug)
			System.out.println(String.format("Focus position: X=%f, Y=%f, Z=%f.", selectedPosition.getX(),selectedPosition.getY(),selectedPosition.getZ()));
		
		return true;
		
	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (selectedPosition==null)
			return o;
		
		PointRoi pRoi=new PointRoi(selectedPosition.getX(), selectedPosition.getY());
		pRoi.setPosition(selectedPosition.getZ().intValue()+1);
		o.add(pRoi);
		
		if (boxRoi!=null)
			o.add(boxRoi);
			
		return o;
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
		if (selectedPosition!=null)
			img.setPosition(selectedPosition.getZ().intValue()+1);
		IJ.run(img, "Enhance Contrast", "saturated=0.35");
		

	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", selectedPosition);
		

		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
	}
	
	@Override
	public void  postProcessFail()throws Exception{
		//super.postProcessFail();
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-HZ", new Point3D[0]);
		
		IJ.log("No focus point was selected.");

		
	}
	
	protected void postProcessOffline(){
		try {
			this.visualise(1000, 20);;
		}catch (Exception ex){
			IJ.error("Error during offline visualisation");
		}
	}


	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_WAIT_IMAGE_OPENING,	null, imageOpeningTimeDelay,ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_USE_TEMPLATE_MATCHING,null, useTemplateMatching,	ParameterType.BOOL_PARAMETER);
		jobCollection.addParameter(KEY_TEMPLATE_PROBABILITY_THRESHOLD,null, probabilityThreshold,	ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_TEMPLATE_FILE_PATH,	null, templateFilePath,	ParameterType.FILEPATH_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.imageOpeningTimeDelay=	(Integer)_jobParameterCollection.getParameterValue(KEY_WAIT_IMAGE_OPENING);
		this.useTemplateMatching=	(Boolean)_jobParameterCollection.getParameterValue(KEY_USE_TEMPLATE_MATCHING);
		this.probabilityThreshold=	(Double)_jobParameterCollection.getParameterValue(KEY_TEMPLATE_PROBABILITY_THRESHOLD);
		this.templateFilePath=		(String)_jobParameterCollection.getParameterValue(KEY_TEMPLATE_FILE_PATH);

	}
	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		final String pluginDir="D:\\Alex_work\\running_soft\\Fiji.app_2019\\plugins";
		//final String pluginDir="/home/cosenza/Programs/fiji-linux64/Fiji.app/plugins";
		
		//final String tblAPth="D:\\tempDat\\Marco_Cosenza\\test-micronuclei-identification\\summary_manual.txt";
		final String tblAPth="W:\\kletter\\20200728\\20200728-172656/summary_.txt";

		//Images with detectable Spindles are:
		final String imgFnm="AFHighZoom--W0000--P0002-T0001--0001.czi";
		
		System.getProperties().setProperty("plugins.dir", pluginDir);
		
		//new ImageJ().ui().showUI();
		
		new ij.ImageJ();
		
		Job_FocusSpindle testJob=new Job_FocusSpindle();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}
	
}
