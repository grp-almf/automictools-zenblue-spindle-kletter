package automic.online.jobs.segment;

import java.io.File;
//import java.util.Random;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissLSM800;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
//import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.frame.RoiManager;
import ij.process.ImageStatistics;




public class Job_Segment_Spindle_Kletter_TemplateMatch extends Job_Default{
	public static final String INPUT_IMG_TAG="LowZoomStitch";
	
	private boolean selectionMade=false;
	

	public static final String KEY_CHANNEL_INDEX="Channel index";
	public static final String KEY_TEMPLATE_IMAGE_PATH="Template image path";

	public static final String KEY_PROBABILITY_THRESHOLD="Probability threshold";
	public static final String KEY_OVERLAP="Overlap";
	public static final String KEY_MAX_OBJECTS="Maximum number of objects";

	
	
	private ImagePlus inImg;
	ImagePlus maxProjectedImage;
	

	//PIPELINE PARAMETERS
	private int chanelIndex=1;
	
	//min area in pixels
	double overlap = 0.2;
	
	double probabilityThreshold=0.79;
	
	int maxNObjects=5;
	
	
	//template images
	String templateImagePath = "D:\\tempDat\\tobias_kletter_test\\LZ-templates\\Spindle_template_2.tif";
	
	Roi[] objectRois;
	Point3D[] positions;

	
	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_Segment_Spindle_Kletter_TemplateMatch(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		selectionMade=false;
		positions=null;
		objectRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		//currentTable.cleanRecord(curDInd);

		TimeUnit.MILLISECONDS.sleep(3000);
		inImg=ImageOpenerWithBioformats.openSingleChannel(newImgFile,0,chanelIndex,false);
		//selectedRoiManager= new SelectionManadgement();
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		inImg=ImageOpenerWithBioformats.openSingleChannel(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"),0,chanelIndex,false);
		//selectedRoiManager= new SelectionManadgement();
	}


	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		
		// removing scale to count everything in pixels
		IJ.run(inImg, "Set Scale...", "distance=0");
		
		showDebug(inImg, "OriginalImage", true);

		//Duplicate only first channel
		ImagePlus inImgCh1=new Duplicator().run(inImg, 1, 1, 1, inImg.getNSlices(), 1, inImg.getNFrames());
		
		//Z projection (if needed)
		if (inImg.getNSlices() > 1) {
			
			ZProjector projector=new ZProjector();
			projector.setMethod(ZProjector.MAX_METHOD);
			projector.setImage(inImgCh1);
			projector.doProjection();
			maxProjectedImage = projector.getProjection();
					
		} else {
			maxProjectedImage = inImgCh1;
		}
		showDebug(maxProjectedImage, "Max Projected Image", true);
		
		maxProjectedImage.setTitle("MaxProjectedImage");
		maxProjectedImage.show();
		
		ImagePlus tempImage=IJ.openImage(templateImagePath);
		tempImage.show();
		tempImage.setTitle("TempImage");

		
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		IJ.run("Template Matching Image",String.format("template=TempImage image=MaxProjectedImage flip_template_vertically flip_template_horizontally rotate=[30, 60, 90, 120, 150] matching_method=[Normalised 0-mean cross-correlation] number_of_objects=20 score_threshold=%f maximal_overlap=%f add_roi", probabilityThreshold,overlap));
		maxProjectedImage.hide();
		tempImage.hide();
		
		
		objectRois=rm.getRoisAsArray();
		
		if(ROIManipulator2D.isEmptyRoiArr(objectRois))
			return false;
		
		if (objectRois.length>maxNObjects) {
			ParticleFilterer pFilterer =new ParticleFilterer(maxProjectedImage.getProcessor(), objectRois);
			pFilterer.selectRandomObjects(maxNObjects);
			objectRois=pFilterer.getPassedRois();
		}
		
		int nObjects=objectRois.length;
		positions=new Point3D[nObjects];
		for (int iObject=0;iObject<nObjects;iObject++) {
			maxProjectedImage.setRoi(objectRois[iObject]);
			ImageStatistics stat=maxProjectedImage.getStatistics(ImageStatistics.CENTROID);
			positions[iObject]=new Point3D(stat.xCentroid,stat.yCentroid,null);
		}
		
		return true;
	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if(!ROIManipulator2D.isEmptyRoiArr(objectRois))
			for (Roi r:objectRois)
				o.add(r);
		
		
		
		
		return o;
	}
	

	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(maxProjectedImage, getOverlay(), _xvis, _yvis);

		//if (selPoint==null) return;
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", positions);

		File roiSaveFolder=new File(newImgFile,"../../Metaphase.Roi").getCanonicalFile();
		if (!roiSaveFolder.isDirectory()) {
			roiSaveFolder.mkdir();
		}
		String roiNameNoExt=FileUtils.cutExtension(newImgFile.getName());
		ROIManipulator2D.saveRoisToFile(roiSaveFolder.getAbsolutePath(), roiNameNoExt, objectRois);
		currentTable.setFileAbsolutePath(new File(roiSaveFolder,roiNameNoExt+".zip"), curDInd, "Metaphase.Roi", "ROI");
		
		/*
		selectedRoiManager.updateCommandStrings();
		ZeissKeys.submitCommandsToMicroscope("trigger1",""+(inImg.getWidth()/2.0-1),""+(inImg.getHeight()/2.0-1),"0",selectedRoiManager.roiTypeCommandString,selectedRoiManager.roiAimCommandString, selectedRoiManager.xCommandString, selectedRoiManager.yCommandString,"");
		IJ.log(String.format("Launcing photomanipulation imaging job with %d selected regions", this.selectedRoiManager.selectedRegions.size()));
		
		int nRois=selectedRoiManager.selectedRegions.size();
		if (nRois>0) {
			Roi[] selectedRegionsArray=selectedRoiManager.selectedRegions.toArray(new Roi[nRois]);
		
			saveRoisForImage(newImgFile, selectedRegionsArray);
		}
		*/
		
		
	}
	
	@Override
	public void postProcessFail()throws Exception{
		//super.postProcessFail();
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", positions);
		
		
		IJ.log("No region was selected. Continue with selection imaging job");

	}

	@Override
	public void postProcessOffline(){
		try {
			ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", positions);
		}catch(Exception ex) {
			System.out.println("Exception");
		}
		IJ.log("Selection made "+selectionMade);
		//if (selectionMade)
		//	IJ.log("Number of selected ROIs " + this.selectedRoiManager.selectedRegions.size());
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_CHANNEL_INDEX, null, chanelIndex, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_TEMPLATE_IMAGE_PATH, null, templateImagePath, ParameterType.STRING_PARAMETER);
		
		jobCollection.addParameter(KEY_PROBABILITY_THRESHOLD, null, probabilityThreshold, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_OVERLAP, null, overlap, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_MAX_OBJECTS, null, maxNObjects, ParameterType.INT_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){

		this.chanelIndex=(Integer)_jobParameterCollection.getParameterValue(KEY_CHANNEL_INDEX);
		this.templateImagePath=(String)_jobParameterCollection.getParameterValue(KEY_TEMPLATE_IMAGE_PATH);

		this.probabilityThreshold=(Double)_jobParameterCollection.getParameterValue(KEY_PROBABILITY_THRESHOLD);
		this.overlap=(Double)_jobParameterCollection.getParameterValue(KEY_OVERLAP);
		this.maxNObjects=(Integer)_jobParameterCollection.getParameterValue(KEY_MAX_OBJECTS);
		
		
	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-3, 10,10,3);
	}

	
	
	
	public static void main(String[] args)throws Exception{
		// start ImageJ
		//plugin directory needed for certain MorphoLibJ operations, such as Distance Transform Watershed

		final String pluginDir="D:\\Alex_work\\running_soft\\Fiji.app_2019\\plugins";
		//final String pluginDir="/home/cosenza/Programs/fiji-linux64/Fiji.app/plugins";
		
		//final String tblAPth="D:\\tempDat\\Marco_Cosenza\\test-micronuclei-identification\\summary_manual.txt";
		final String tblAPth="D:\\tempDat\\tobias_kletter_test\\20200724-201611sub/summary_m.txt";

		//Images with detectable Spindles are:
		final String imgFnm="LowZoomStitch--W0000--P0002-T0093.czi";
		
		System.getProperties().setProperty("plugins.dir", pluginDir);
		
		//new ImageJ().ui().showUI();
		
		new ij.ImageJ();
		
		Job_Segment_Spindle_Kletter_TemplateMatch testJob=new Job_Segment_Spindle_Kletter_TemplateMatch();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}
	

}
