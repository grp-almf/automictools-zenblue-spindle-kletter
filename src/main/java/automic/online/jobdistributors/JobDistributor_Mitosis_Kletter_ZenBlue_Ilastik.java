package automic.online.jobdistributors;


import ij.IJ;
import ij.ImageJ;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;

import automic.online.jobdistributors.JobDistributor_Abstract;
import automic.online.jobs.Job_Default;
import automic.online.jobs.common.Job_AutofocusInit_ZenBlue;
import automic.online.jobs.common.Job_Autofocus_ZenBlue;
import automic.online.jobs.common.Job_RecordFinish;
import automic.online.jobs.segment.Job_FocusSpindle;
import automic.online.jobs.segment.Job_Segment_Spindle_Kletter_Ilastik;
import automic.table.TableModel;
import automic.table.TableProcessor;


public class JobDistributor_Mitosis_Kletter_ZenBlue_Ilastik extends JobDistributor_Abstract implements PlugIn {
	

	
	@Override
	protected void fillJobList(){
		
		
		
		super.addImageJob(Job_AutofocusInit_ZenBlue.class,	"AF--",  "AF",	true);
		super.addImageJob(Job_Default.class,	"LowZoom--", "LowZoom",	true);
		super.addImageJob(Job_Segment_Spindle_Kletter_Ilastik.class,	"LowZoomStitch--", "LowZoomStitch",	true);
		super.addImageJob(Job_FocusSpindle.class,	"AFHighZoom--",  "AFHighZoom",	true);
		super.addImageJob(Job_RecordFinish.class,	"HighZoom--", "HighZoom",	true);
	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");
		
		outTbl.addFileColumns("AF", "IMG");
		outTbl.addFileColumns("LowZoom", "IMG");
		outTbl.addFileColumns("LowZoomStitch", "IMG");
		outTbl.addFileColumns("AFHighZoom", "IMG");
		outTbl.addFileColumns("HighZoom", "IMG");
		outTbl.addFileColumns("Metaphase.Roi", "ROI");

		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected TableProcessor configureTableProcessor(TableModel _tModel)throws Exception{
		TableProcessor tProcessor=new TableProcessor(_tModel);
		return tProcessor;
	}

	
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}


	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="C:/tempDat/AutoMic_test";
		this.setGeneralOptions(searchPath, true, false);
		//this.fileExtension="czi";
	}
	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = JobDistributor_Mitosis_Kletter_ZenBlue_Ilastik.class;
		//String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		//String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		//System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();
		//DEBUG=true;
		//Debug.run(plugin, parameters);
		//new WaitForUserDialog("test Maven project").show();
		
		// run the plugin
		
		//exrerimentFolderPath="D:/tempDat/AutoFRAP_test";
		//showDemo=true;
		//pnum=1;
		//setDebugConfiguration();
		

		IJ.runPlugIn(clazz.getName(),"Debug run");
		//IJ.runPlugIn(clazz.getName(),"");

	}
	
}
